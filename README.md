unClone
=======

Description
-----------

Finds duplicate files in one or more provided directories. Optionally
ignores (sym)links.

It works with both python 2 and 3.


Usage
-----

Simply run

    python run.py first/path second/path

This will output paths to identical files, one on each line, each batch
separated by an additional blank line. Links are marked with `[link]` on
the end.


Inner Workings
--------------

The scanner simply calculates the hash (sha512) of the first few bytes
of each non-empty file, saving any hash-matches for later. Then it
discards unmatched files and calculates the hashes again for the
remaining files, but this time for the entire file. This two-pass
system is meant to reduce overall i/o, since most files will be
discared after only reading a small portion.
