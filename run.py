#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2013-2014 by Kirill Stepanov <kirill.v.stepanov@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from __future__ import print_function

from os.path import join, getsize, islink, exists
from collections import defaultdict
from hashlib import sha512
from os import walk


class UnClone(object):
    """Class to walk 1 or more paths to check for duplicate files."""

    # how many bytes to read for the initial check
    # 1k seems to be the smallest amount that doesn't impact performance
    HEADER_CHECK_SIZE = 2 ** 10

    # prevent excess memory use with large files
    CHUNK_READ_SIZE = 64 * 2 ** 10

    HASH_FUNCTION = sha512


    def __init__(self, paths, ignore_links=False):
        """
        Constructor.

        paths           list of paths
        ignore_links    ignore links when looking for dupes?"""

        self.paths = paths
        self.ignore_links = ignore_links

        self.results = None
        self.empty_files_ignored = None
        self.total_files_scanned = None


    def run(self):
        """Run the program to scan given dirs. Return list of lists."""

        self.empty_files_ignored = 0
        self.total_files_scanned = 0

        files = self.get_potential_files()
        files = self.hash_screening(files, self.HEADER_CHECK_SIZE)
        files = self.hash_screening(files)

        self.results = files


    def get_potential_files(self):
        """
        Scan paths one by one and group files by size.

        Return list of non-unique file sizes."""

        output = defaultdict(list)

        for arg_path in self.paths:
            for path, dirnames, filenames in walk(arg_path):
                for filename in filenames:
                    filepath = join(path, filename)

                    if self.ignore_links and islink(filepath):
                        continue
                    elif not exists(filepath):
                        # avoid broken symlinks
                        continue

                    size = getsize(filepath)
                    self.total_files_scanned += 1

                    if size == 0:
                        # we have no interest in empty files
                        self.empty_files_ignored += 1
                        continue
                    else:
                        # or append to existing
                        output[size].append(filepath)

        # filter out unique file sizes and return
        return (files for files in output.values() if len(files) > 1)


    def hash_screening(self, files, read_bytes=None):
        """
        Check the files by their hashsum.

        If `bytes` provided, scan only that many bytes. All by default."""

        output = []

        for possible_matches in files:
            tmp = defaultdict(list)

            for path in possible_matches:
                digest = self._calculate_digest(path, read_bytes)
                tmp[digest].append(path)

            # filter out unique hashes
            output.extend(x for x in tmp.values() if len(x) > 1)

        return output


    @classmethod
    def _calculate_digest(cls, path, read_bytes):
        """Calculate hash digest of given file."""

        hash_buffer = cls.HASH_FUNCTION()
        chunk_size = read_bytes or cls.CHUNK_READ_SIZE

        with open(path, 'rb') as fp:
            while True:
                data = fp.read(chunk_size)
                hash_buffer.update(data)

                if read_bytes or not data:
                    break

        return hash_buffer.hexdigest()


if __name__ == '__main__':
    """If running file directly."""

    from os.path import expanduser
    from sys import stderr
    import argparse

    args = argparse.ArgumentParser(description='Find duplicate files.',
                                   usage='dedup.py [OPTION...] [PATH...]')

    args.add_argument('paths', default=['.',], nargs='*')
    args.add_argument('-i', '--ignore-links', default=False,
                      action='store_true',
                      help='don\'t consider symlinks duplicates')

    arguments = args.parse_args()

    paths = {expanduser(x) for x in arguments.paths}
    bad_paths = {x for x in paths if not exists(x)}
    paths -= bad_paths

    for path in bad_paths:
        print('Warning: ignoring invalid path - ' + path, file=stderr)


    # conditions met, ready for take off
    d = UnClone(paths, ignore_links=arguments.ignore_links)
    d.run()

    for i, files in enumerate(d.results):
        if i != 0: # don't print extra new line
            print()
        for f in files:
            print(f, end='')
            # add [link] at end of file names if they are links
            if not arguments.ignore_links and islink(f):
                print(' [link]', end='')
            print()
